<?php
namespace database\model;
include "database/Connection.php";

use database\Connection;

abstract class AbstractModel
{
    private $id;

    private $values = array();

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    public function setValue($fieldName, $fieldValue) {
        $this->values[$fieldName] = $fieldValue;
    }

    public function save()
    {
        if (!$this->id) {
            $this->insertModel();
        } else {
            $this->updateModel();
        }
    }

    private function insertModel()
    {
        //В идеале
        //1)Старт транзакции
        //2)Считываем последний ID
        //3)Инсертим
        //4)Коммитим

        $query = "INSERT into " . $this->getTableName() . " (" . join(",", $this->getFieldNames()) . ") VALUES (" . $this->getValueString() . ")";
        $conn = Connection::getInstance()->getConnection();
        $result = $conn->query($query);
        if(!$result) {
            //TODO: ROLLBACK transaction
            //TODO: throw exception
            //TODO: delete echo
            echo $conn->error;
        }
    }

    private function updateModel()
    {

    }

    abstract protected function getTableName();

    abstract protected function getFieldNames();

    /**
     * Подготавливает знаачения из объекта для сохранения в БД
     *
     * @return String Строка значений из объекта
     */
    private function getValueString()
    {
        $result = "";
        $first = true;
        foreach ($this->getFieldNames() as $fieldName) {
            if($first){
                $first = false;
            } else {
                $result .= ",";
            }
            $result .= '\'' . $this->values[$fieldName] . '\'';
        }
        return $result;
    }
}