<?php

namespace database\model;
include "AbstractModel.php";

class User extends AbstractModel
{

    const TABLE_NAME = 'users';

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getValues()["name"];
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->setValue("name", $name);
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->getValues()["email"];
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->setValue("email", email);
    }

    /**
     * @return mixed
     */
    public function getRegistrationDate()
    {
        return $this->getValues()["registration_date"];
    }

    /**
     * @param mixed $registration_date
     */
    public function setRegistrationDate($registration_date)
    {
        $this->setValue("registration_date", $registration_date);
    }

    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->getValues()["role_id"];
    }

    /**
     * @param mixed $role_id
     */
    public function setRoleId($role_id)
    {
        $this->setValue("role_id", $role_id);
    }


    protected function getTableName()
    {
        return self::TABLE_NAME;
    }

    protected function getFieldNames()
    {
        return self::getFieldNamesStatic();
    }

    public static function getFieldNamesStatic()
    {
        $result = [
            "name",
            "email",
            "registration_date",
            "role_id"];
        return $result;
    }
}